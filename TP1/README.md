# Laboratorio de Microprocesadores, segundo cuatrimestre 2022

## Trabajo Práctico 1: manejo de puertos

Los objetivos de este trabajo práctico son familizarse con un ambiente de desarrollo de microcontrolador, usar los registros de los puertos y ver utilidad del resistor de _pullup_ interno.

## Actividades
1. Investigar cuáles son los métodos para programar el microcontrolador atmega328. Características de cada método. ¿Cuál de esos métodos utilizará en este Trabajo Práctico? (depende del hardware seleccionado).
2. Hacer un programa que haga parpadear un LED, usando la rutina de retardo dada.
3. Modificar el programa de modo que que al presionar el pulsador 1 el led comience a parpadear y quede parpadeando hasta que se presione el pulsador 2. 
4. Modificar el circuito y el programa anterior, para reemplazar los resistores de _pullup_ conectados a los pulsadores por los resistores de _pullup_ internos que posee el microcontrolador.

## Materiales

* 1 LED 
* 1 resistores de 220 Ohms
* 2 resistores de 10 Kohms 
* 2 pulsadores
* 1 microcontrolador ATmega328p
* 1 circuito programador (Club de Robótica/Arduino/etcétera).

## Circuito esquemático



![Esquemático](/esquematico_tp1.jpg "Esquemático TP1")


