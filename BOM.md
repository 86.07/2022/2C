# Lista de materiales Trabajos Prácticos Segundo Cuatrimestre 2022

* Alternativa plataforma propia: este enfoque visibiliza las partes que componen el hardware.

| Cantidad |                 Dispositivo               |               Comentario             | Posibles links de compra online |
|----------|-------------------------------------------|--------------------------------------|---------------------------------|
| 1        | módulo programador para MCUs AVR          | consultar en el LABI si venden programador CdR | [CdR](http://clubderobotica.com.ar/) [Nubbeo](https://www.nubbeo.com.ar/productos/programador-microcontrolador-atmel-avr-usbasp-usb-asp-nubbeo/) | 
| 1        | fuente de alimentación 5V 1A              | cargador de celular en desuso?       | |
| 1        | protoboard 830 puntos	                   |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/protoboard-de-830-puntos-experimentador-circuitos-nubbeo/) | 
| 1        | display 7 segmentos, ánodo o cátodo común | un dígito o dos, salen casi lo mismo | [Nubbeo](https://www.nubbeo.com.ar/productos/display-7-segmentos-digito-0-56-rojo-catodo-comun-nubbeo/)
| 1        | MCU Atmega328p montaje DIP                |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/atmega328-atmega328p-pu-dip28-bootloader-arduino-nubbeo/) |
| 1        | Sensor de sonido                          |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/modulo-sensor-de-sonido-microfono-regulable-arduino-nubbeo/) |
| 1        | Potenciómetro lineal 10K                  |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/potenciometro-10k-ohms-lineal-pote-arduino-nubbeo/) |
| 1        | pack cables Dupont macho-macho            | Un pack puede compartirse            | [Nubbeo](https://www.nubbeo.com.ar/productos/pack-40-cables-30cm-protoboard-macho-macho-dupont-nubbeo/)
| 10       | resistencias 220 o 330 5% 1/4W            |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/pack-10-resistencias-330-ohm-1-4w-resistencia-arduino-nubbeo/)
| 10       | resistencias 10k 5% 1/4W                  |                                      | 
| 2        | pulsadores DIP (tact switch)              |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/pack-5-boton-pulsador-tecla-tact-switch-12mm-x-12mm-x-7-3mm-nubbeo/)


* Alternativa Arduino: este enfoque utiliza el bootloader Arduino para programar el MCU

| Cantidad |                 Dispositivo               |               Comentario             | Posibles links de compra online |
|----------|-------------------------------------------|--------------------------------------|---------------------------------|
| 1        | fuente de alimentación 5V 1A              | cargador de celular en desuso?       | |
| 1        | protoboard 830 puntos	                   |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/protoboard-de-830-puntos-experimentador-circuitos-nubbeo/) | 
| 1        | display 7 segmentos, ánodo o cátodo común | un dígito o dos, salen casi lo mismo | [Nubbeo](https://www.nubbeo.com.ar/productos/display-7-segmentos-digito-0-56-rojo-catodo-comun-nubbeo/)
| 1        | Arduino UNO o Nano                        |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/arduino-nano-v3-0-atmega328-compatible-usb-con-ch340-nubbeo/) |
| 1        | Cable microUSB (UNO) o mini USB (Nano)    |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/cable-mini-usb-para-arduino-nano-30cm-miniusb-arduino-nubbeo/)
| 1        | Sensor de sonido                          |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/modulo-sensor-de-sonido-microfono-regulable-arduino-nubbeo/) |
| 1        | Potenciómetro lineal 10K                  |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/potenciometro-10k-ohms-lineal-pote-arduino-nubbeo/) |
| 1        | pack cables Dupont macho-macho            | Un pack puede compartirse            | [Nubbeo](https://www.nubbeo.com.ar/productos/pack-40-cables-30cm-protoboard-macho-macho-dupont-nubbeo/)
| 10       | resistencias 220 o 330 5% 1/4W            |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/pack-10-resistencias-330-ohm-1-4w-resistencia-arduino-nubbeo/)
| 10       | resistencias 10k 5% 1/4W                  |                                      | 
| 2        | pulsadores DIP (tact switch)              |                                      | [Nubbeo](https://www.nubbeo.com.ar/productos/pack-5-boton-pulsador-tecla-tact-switch-12mm-x-12mm-x-7-3mm-nubbeo/)


