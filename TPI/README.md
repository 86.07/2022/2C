# Laboratorio de Microprocesadores, segundo cuatrimestre 2022


## Trabajo Práctico Integrador: decodificador Morse

El objetivo de este último trabajo práctico es desarrollar un dispositivo capaz de intepretar un mensaje sonoro codificado en Morse, mostrando el mensaje decodificado en una pantalla de computadora.
El mensaje Morse sonoro provendrá de una aplicación de celular o computadora a elección.

### Decodificador de audio

Este módulo provee una salida digital, que se enciende según el conjunto micrófono amplificador detectan sonido mayor a un umbral, es decir:

* Presencia de audio => "1" lógico
* Ausencia de audio  => "0" lógico

El umbral se calibra mediante un preset multivuelta en el módulo, el cual deberá ajustarse con un tono de referencia, girandolo con suavidad para evitar que se rompa. El esquemático del detector de sonido forma parte también del esquemático general y formará parte del informe.

### Alcance del firmware

* El firmware a desarrollar medirá la duración de los pulsos y los silencios usando timers. Al detectarse el final de cada caracter Morse, este deberá decodificarse y enviarse por puerto serie del microcontrolador. 

* El puerto serie estará conectado a un adaptador serie a USB y este último a la computadora. De esem modo, utilizando un terminal de comunicaciones, la computadora mostrará los caracteres ASCII que el microconotrolador transmita.

* La duración de los símbolos no es fija, sino que quedará determinada por una calibración inicial que realice el firmware, midiendo el tiempo de los primeros pulsos recibidos. 

* Se asumirá que esa duración, vinculada con la velocidad de transmisión, es estable en el tiempo, ya que para generar los mensajes se usará alguna fuente fiable que transmita Morse a velocidad constante. Es decir, la duración de los puntos, las rayas y los silencios será constante a lo largo de la ejecución del programa. 

### Acerca del código Morse

La recomendación de ITU [M.1677-1](https://www.itu.int/dms_pubrec/itu-r/rec/m/R-REC-M.1677-1-200910-I!!PDF-E.pdf) especifica además del código, la duración relativa de tiempos:

- El punto es el menor tiempo posible
- Una raya dura como tres puntos
- El silencio entre símbolos de una letra dura un punto
- El silencio entre dos letras dura tres puntos
- El silencio entre dos palabras dura siete puntos

### Recomendaciones de implementación

Para decodificar un caracter, se sugiere armar tablas en flash con los códigos a traducir y recorrer dichas tablas hasta encontrar la concordancia. Puede ser conveniente tener varias tablas, según la cantidad de puntos y rayas. Es decir:

- Una tabla que contenga solamente los caracteres que tienen exactamente un punto o raya
- Una tabla que contenga solamente los caracteres que contienen exactamente dos puntos o rayas
- Una tabla que contenga solamente los caracteres que contienen exactamente tres puntos o rayas
- ... etcétera.

Para generar los mensajes de audio en código Morse, se puede buscar 'Morse generator' en los _market places_ de Android/iPhone/Windows/Linux/Mac. 


