# Laboratorio de Microprocesadores, segundo cuatrimestre 2022


## Trabajos Prácticos

- [Lista de materiales](https://gitlab.com/86.07/2022/2C/-/blob/main/BOM.md)
- [Trabajo Práctico 1](https://gitlab.com/86.07/2022/2C/-/blob/main/TP1/README.md)
- [Trabajo Práctico 2](https://gitlab.com/86.07/2022/2C/-/blob/main/TP2/README.md)
- [Trabajo Práctico 3](https://gitlab.com/86.07/2022/2C/-/blob/main/TP3/README.md)
- [Trabajo Práctico integrador](https://gitlab.com/86.07/2022/2C/-/blob/main/TPI/README.md)


## Requerimientos del informe

### Carátula

Debe incluir el número del proyecto, turno elegido, fecha de presentación, nombre del alumno con su número de padrón correspondiente y de los docentes a cargo del turno.

### Numeración

Se deberá numerar cada página del proyecto e indicar en cual folio se encuentra cada item tratado. También se detallarán los apéndices agregados. De ser posible, se deberá numerar cada renglón dentro de la página. Esto simplifica el proceso de evaluación.

### Objetivo

En el inicio del informe, se debe explicitar los objetivos y del trabajo práctico. 

### Descripción

En este punto se desarrollará una explicación de los objetivos y funcionamiento del trabajo. 


### Diagrama de bloques y circuito esquemático·

El diagrama de bloques debe describir los componentes de hardware a nivel macro.

El circuito esquemático debe respresentar fielmente el hardware utilizado, debe estar *completo*. Es decir, si por ejemplo se usa una placa Arduino u otro módulo prearmado, el esquemático de dicha placa forma parte del circuito del trabajo práctico y por lo tanto *debe* figurar en el esquemático documentado. El estudiante debe comprender funcionalmente *la totalidad* del circuito utilizado. Esto significa que el esquemático debe mostrar todos los componentes electrónicos, no un bloques funcionales que representen un módulo Arduino.

_Tener en cuenta que el esquemático de Arduino es de dominio público, con lo cual se puede descargar de Internet y modificar con los componentes usados en el trabajo práctico._ 

### Listado de componentes y tabla de gastos

Se listarán los componentes utilizados y la lista de precios, totalizando un precio final de la tabla.

### Firmware

Para documentar la implementación realizada, se debe incluir:

* Diagrama de flujo, tal que permitan entender rápidamente la lógica (omitir detalles de implementación, es un esquema macro)
* Código fuente del programa. Los fuentes deben estar comentados adecuadamente. En particular, incluir prototipos de cada función
* Si se utiliza cualquier software o ambiente de desarrollo, indicar el nombre.

### Resultados y conclusiones

*Criterio de decisión: como regla general para cada sección del informe, se debe justificar las decisiones tomadas.*

En esta sección se hará una evaluación acerca de los alcances. Se detallarán los resultados obtenidos: 

* Si hay decisiones que involucran cálculos, incluirlos. 
* Si aplica, análizar el consumo de corriente.
* Comentar no solamente los resultados obtenidos, sino también los obstáculos que se presentaron y cómo se resolvieron. 

### Entregables:

Todo el material escrito se entrega en formato pdf.

El proyecto de firmware, si bien figura en pdf, se vuelve a entregar en digital, de modo de poder ejecutarlo. Por ejemplo, si es un proyecto en Atmel Studio, comprimir el directorio completo.

Incluir un video del proyecto andando como link a YouTube, usando la modalidad de video no listado.


