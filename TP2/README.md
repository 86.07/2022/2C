# Laboratorio de Microprocesadores, segundo cuatrimestre 2022

## Trabajo Práctico 2: medición de intervalo de tiempos, puerto serie e interrupciones

Los objetivos de este trabajo práctico son:
1. Comprender cómo medir intervalos de tiempo el microcontrolador
2. Usar el puerto serie como una herramienta para debug

## Actividades
1. Investigar cómo funciona el modo de medición de intervalo de tiempo usando el Timer 1
2. El programa a implementar deberá detectar la duración de los pulsos de entrada en el pin ICP1 (PB0). 
- Si el pulso en nivel alto es menor que 200 milisegundos, prender el punto del display de 7 segmentos. 
- Si el pulso en nivel alto es mayor que 200 milisegundos, prender cualquier barra de dicho display.
- Cuando el pulso en nivel bajo es mayor que 200 milisegundos, apagar todos los LEDs.

La decisión sobre qué LED encender se deberá tomar al finalizar el pulso. Es decir, se empieza a medir tiempo en el flanco ascendente y se termina de medir en el flanco descendente.



Notas: 
- Los pulsos se deben generar con el instrumental disponible en el laboratorio o en el pañol. Para evitar quemar el pin del microcontrolador, se sugiere colocar una resistencia en serie entre el generador y la entrada ICP1. De todos modos, prestar atención, aplicando siempre niveles lógicos compatibles. 
- Escribir el programa usando el template provisto para este trabajo práctico, el cual configura el puerto serie (USART) del microcontrolador. Se puede usar la transmisión y recepción de puerto serie para facilitar la desarrollo, ya que se puede imprimir trazas y verlas en la pantalla de la PC.
- Implementar el programa bajo dos modalidades:
* a) polling del flag ICF1
* b) interrupciones

## Materiales

* 1 display de 7 segmentos
* 2 resistores de 220 Ohms
* 1 resistor de 10KOhms
* Generador de audio frecuencia, o generador de funciones, o generador de pulsos.
* 1 microcontrolador ATmega328p
* 1 circuito programador (Club de Robótica/Arduino/etcétera).
