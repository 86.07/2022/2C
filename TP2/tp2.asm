; Para realizar el Trabajo Pr�ctico, utilizar este template como base. 
 

.equ USART_BAUDRATE = 115200            ; velocidad de datos de puerto serie (bits por segundo)
.equ F_CPU = 16000000                	; Frecuencia de oscilador, Hz (Arduino UNO: 16MHz, Oscilador interno: 8MHz)

.include "m328Pdef.inc"
.include "usart.inc"

.cseg

.org 0x0000
	rjmp	onReset
;**************************************************************
;* Punto de entrada al programa 
;**************************************************************
  
onReset:
; Inicializa el "stack pointer"
	ldi		r16, LOW(RAMEND)   
  	out		spl, r16
	ldi    	r16, HIGH(RAMEND)   
	out    	sph, r16

; Inicializa el puerto serie (USART)
  	rcall  	usart_init                   
  	
; Habilitaci�n global de interrupciones
	sei

; Loop infinito
mainLoop:
    rjmp    mainLoop

; Esta subrutina se invoca cada vez que se recibe un dato por puerto serie
; Para ello, usar un terminal serie de computadora.
; Por ejemplo, Realterm:
; https://sourceforge.net/projects/realterm/files/Realterm/2.0.0.70/Realterm_2.0.0.70_setup.exe/download

usart_callback_rx:
; En este ejemplo, el dato recibido en r16 se vuelve a trasmitir, es decir, hace un "eco"
; Se puede modificar esta rutina de acuerdo a la necesidad. 
; Por ejemplo: si se recibe la letra 'a', prender un led, etc�tera.
	call	usart_tx
	ret
